Rails.application.routes.draw do
	root 'users#new'

	get     'login' => 'sessions#new'
	delete  'logout' => 'sessions#destroy'


	resources :sessions, only: [:new, :create, :destroy]
	resource :users, only: [:new, :create]
end
